package opath

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

type Path struct {
	parts      []string
	isAbsolute bool
}

func NewPath(pathAsString string) *Path {
	splits := strings.Split(pathAsString, "/")
	if strings.HasPrefix(pathAsString, "/") {
		return &Path{splits[1:], true}
	} else {
		if splits[0] == "" {
			return &Path{[]string{}, false}
		}
		return &Path{splits[:], false}
	}
}

// Resolved updates the path in-situ and returns the same path,
// so repeated call sequentially append
// See Resolve to get a new Path each time
func (p *Path) Resolved(name string) *Path {
	p.parts = append(p.parts, name)
	return p
}

// Resolve creates new path with name added to end.
// See Resolved if you want the actual Path modified
func (p *Path) Resolve(name string) *Path {
	return &Path{append(p.parts, name), p.isAbsolute}
}

// Resolve creates new path with name added to end.
// See Resolved if you want the actual Path modified
func (p *Path) ResolveWithPath(subPath *Path) *Path {
	return &Path{append(p.parts, subPath.parts...), p.isAbsolute}
}

// parent returns a new Path without the last element in Path,
// data is a slice on original, and so changing the original will still
// impact the returned value
func (p *Path) Parent() *Path {
	length := len(p.parts)
	if length > 1 {
		return &Path{p.parts[:length-1], p.isAbsolute}
	}
	return &Path{[]string{}, p.isAbsolute}
}

// child returns a new relative Path without the first element in Path,
// data is a slice on original, and so changing the original will still
// impact the returned value
func (p *Path) Child() *Path {
	length := len(p.parts)
	if length > 1 {
		return &Path{p.parts[1:], p.isAbsolute}
	}
	return &Path{[]string{}, false}
}

func (p *Path) Filename() string {
	l := len(p.parts)
	if l == 0 {
		return ""
	}
	return p.parts[len(p.parts)-1]
}

func (p *Path) Last() string {
	return p.Filename()
}

func (p *Path) String() string {
	if p.isAbsolute {
		return "/" + path.Join(p.parts...)
	} else {
		return path.Join(p.parts...)
	}
}

func (p *Path) Count() int {
	return len(p.parts)
}

// Element returns the path element indicated by 'n'.  If n is negative then
// the path is indexed from the end, thus element -1 is the penultimate element.
// Index 0 is the first element, to get the last one call Filename or Last
//   * param index: the index into the array of path elements
//   * return: If index is out of range then "" else the element
func (p *Path) Element(index int) string {
	if index < 0 {
		if -index > len(p.parts) {
			return ""
		}
		return p.parts[len(p.parts)+index-1]
	}
	if index >= len(p.parts) {
		return ""
	}
	return p.parts[index]
}

func (p *Path) Extension() string {
	f := p.Filename()
	pos := strings.LastIndex(f, ".")
	if pos > -1 {
		return f[pos+1:]
	} else {
		return ""
	}
}

func (p *Path) Open() (*os.File, error) {
	return os.Open(p.String())
}

// ForEach applies the supplied func to each file described by that path.
// If path does not point to a directory an error is returned.
// should this be recursive? - don't have a need so no.
func (p *Path) ForEach(f func(*Path, os.FileInfo) error) error {
	name := p.String()
	var dirs, err = ioutil.ReadDir(name)
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		err = f(p.Resolve(dir.Name()), dir)
		if err != nil {
			return err
		}
	}
	return nil
}

// Exists determines in path actually exists
// should we have isDir and Size as well?
func (p *Path) Exists() bool {
	_, err := os.Stat(p.String())
	if err != nil && os.IsNotExist(err) {
		return false
	}
	fmt.Printf("path %s exists", p.String())
	return true
}
