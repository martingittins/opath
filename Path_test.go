package opath

import (
	"fmt"
	"os"
	"reflect"
	"testing"
)

func TestPathFromString(t *testing.T) {

	var path = NewPath("/this/that/the other/one")
	if !path.isAbsolute {
		t.Errorf("Path should be absolute  %v", path)
	}
	if !reflect.DeepEqual(path.parts, []string{"this", "that", "the other", "one"}) {
		t.Errorf("Path should be absolute  %v", path)
	}
}

func TestPath_Count(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name:   "simple path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: true},
			want:   2,
		},
		{
			name:   "relative path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: false},
			want:   2,
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: true},
			want:   0,
		},
		{
			name:   "null relative path",
			fields: fields{parts: []string{}, isAbsolute: false},
			want:   0,
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			want:   4,
		}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			if got := p.Count(); got != tt.want {
				t.Errorf("Count() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPath_Element(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	type args struct {
		n int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{
			name:   "simple path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: true},
			args:   args{n: 0},
			want:   "Users",
		},
		{
			name:   "relative path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: false},
			args:   args{n: 1},
			want:   "bert",
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: true},
			args:   args{n: 2},
			want:   "",
		},
		{
			name:   "null relative path",
			fields: fields{parts: []string{}, isAbsolute: false},
			args:   args{n: 0},
			want:   "",
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			args:   args{n: 2},
			want:   "noodle farming",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			if got := p.Element(tt.args.n); got != tt.want {
				t.Errorf("Element() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPath_Extension(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020.10.03.txt"}, isAbsolute: true},
			want:   "txt",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			if got := p.Extension(); got != tt.want {
				t.Errorf("Extension() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPath_ForEach(t *testing.T) {

	path := NewPath("/Users/martingittins/Downloads")
	var total int64 = 0
	var largest int64 = 0
	var largestName = ""
	err := path.ForEach(func(path *Path, info os.FileInfo) error {
		total += info.Size()
		if info.Size() > largest {
			largest = info.Size()
			largestName = path.Filename()
		}
		return nil
	})
	if err != nil {
		t.Errorf("ForEach returned errror %v\n", err)
	}
	fmt.Printf("Total is %d\n", total)
	fmt.Printf("Largest item is %s, which is %d bytes\n", largestName, largest)
	if total < 5000 {
		t.Errorf("total is too small %d\n", total)
	}
}

func TestPath_Last(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "simple path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: true},
			want:   "bert",
		},
		{
			name:   "relative path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: false},
			want:   "bert",
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: true},
			want:   "",
		},
		{
			name:   "null relative path",
			fields: fields{parts: []string{}, isAbsolute: false},
			want:   "",
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			want:   "2020",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			if got := p.Last(); got != tt.want {
				t.Errorf("Last() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPath_Resolve(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	type args struct {
		name string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Path
	}{
		{
			name:   "simple path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: true},
			args:   args{name: "ernie"},
			want:   NewPath("/Users/bert/ernie"),
		},
		{
			name:   "relative path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: false},
			args:   args{name: "ernie"},
			want:   NewPath("Users/bert/ernie"),
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: true},
			args:   args{name: "ernie"},
			want:   NewPath("/ernie"),
		},
		{
			name:   "null relative path",
			fields: fields{parts: []string{}, isAbsolute: false},
			args:   args{name: "ernie"},
			want:   NewPath("ernie"),
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			args:   args{name: "ernie"},
			want:   NewPath("/Users/bert/noodle farming/2020/ernie"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			got := path.Resolve(tt.args.name)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Resolve() = %v, want %v", got, tt.want)
			}
			// should not be the original pointer
			if path == got {
				t.Errorf("Orig Path() = %v, want %v", path, got)
			}
		})
	}
}

func TestPath_Resolved(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	type args struct {
		name string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Path
	}{
		{
			name:   "simple path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: true},
			args:   args{name: "ernie"},
			want:   NewPath("/Users/bert/ernie"),
		},
		{
			name:   "relative path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: false},
			args:   args{name: "ernie"},
			want:   NewPath("Users/bert/ernie"),
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: true},
			args:   args{name: "ernie"},
			want:   NewPath("/ernie"),
		},
		{
			name:   "null relative path",
			fields: fields{parts: []string{}, isAbsolute: false},
			args:   args{name: "ernie"},
			want:   NewPath("ernie"),
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			args:   args{name: "ernie"},
			want:   NewPath("/Users/bert/noodle farming/2020/ernie"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			got := path.Resolved(tt.args.name)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Resolved() = %v, want %v", got, tt.want)
			}
			// should be the original pointer
			if path != got {
				t.Errorf("Orig Path() = %v, want %v", path, got)
			}
		})
	}
}

func TestPath_String(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	dirParts := []string{"Users", "bert"}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "simple path",
			fields: fields{parts: dirParts, isAbsolute: true},
			want:   "/Users/bert",
		},
		{
			name:   "relative path",
			fields: fields{parts: dirParts, isAbsolute: false},
			want:   "Users/bert",
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: true},
			want:   "/",
		},
		{
			name:   "null relative path",
			fields: fields{parts: []string{}, isAbsolute: false},
			want:   "",
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			want:   "/Users/bert/noodle farming/2020",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			if got := p.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

type Noodle struct {
	number int
	name   string
}

func TestPath_Noodle(t *testing.T) {
	noodles := make([]*Noodle, 2)
	noodles[0] = &Noodle{number: 1, name: "thin"}
	noodles[1] = &Noodle{number: 2, name: "think"}
	// array indexing makes a copy
	n1 := noodles[0]
	n1.name = "bert"
	fmt.Println(noodles[0].name)

}

func TestPath_parent(t *testing.T) {
	type fields struct {
		parts      []string
		isAbsolute bool
	}
	tests := []struct {
		name   string
		fields fields
		want   *Path
	}{
		{
			name:   "simple path",
			fields: fields{parts: []string{"Users", "bert"}, isAbsolute: true},
			want:   NewPath("/Users"),
		},
		{
			name:   "null path",
			fields: fields{parts: []string{}, isAbsolute: false},
			want:   NewPath(""),
		},
		{
			name:   "complex path",
			fields: fields{parts: []string{"Users", "bert", "noodle farming", "2020"}, isAbsolute: true},
			want:   NewPath("/Users/bert/noodle farming"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			path := &Path{
				parts:      tt.fields.parts,
				isAbsolute: tt.fields.isAbsolute,
			}
			if got := path.Parent(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStuff(t *testing.T) {
	type dyn struct {
		meth  func() int
		value int
	}
	d := dyn{value: 3}
	d.meth = func() int { return 123 }
	d.meth = nil
	if d.meth == nil {
		print("ha ha can't fool me")
	} else {
		y := d.meth()
		print(y)
	}

}
